<?php
error_reporting ( E_ALL | E_STRICT );
ini_set ( 'display_errors', 'On' );
ini_set ( 'max_execution_time', '0' );
set_time_limit(0);
ini_set("log_errors", 1);
ini_set("error_log", "./php-error.log");
error_log( "Hello, errors!" );
ignore_user_abort(true);
$urls = array();
include ('./phpcrawl/crawler.php');
$crawler = new MyCrawler();
$crawler->setWorkingDirectory('./');
$crawler->setURL("abamet.ru");
$crawler->setUrlCacheType(PHPCrawlerUrlCacheTypes::URLCACHE_SQLITE);
$crawler->setConnectionTimeout(20);
$crawler->setStreamTimeout(20);
$crawler->enableResumption();
$crawler->addContentTypeReceiveRule("#text/html#");
$crawler->addURLFilterRule('#(jpg|jpeg|gif|png|get_file|ico|doc|ppt|xlsx|xls|xml|pdf|js|component|css|\?)#i');
//$crawler->addURLFilterRule('#search.php#i');
$crawler->obeyNoFollowTags(true);
$robotsTxtPath = $_SERVER['DOCUMENT_ROOT'] . '/robots.txt';
$crawler->obeyRobotsTxt(true, $robotsTxtPath);
$crawler->enableCookieHandling(true);
$crawler->enableAggressiveLinkSearch(false);
$crawler->excludeLinkSearchDocumentSections(PHPCrawlerLinkSearchDocumentSections::ALL_SPECIAL_SECTIONS);
//$crawler->setTrafficLimit(1000 * 1024 * 1024);
$crawler->setFollowRedirects(true);
$crawler->setFollowRedirectsTillContent(true);
if (!file_exists("crawid.tmp"))
{
	$crawler_id = $crawler->getCrawlerId();
	file_put_contents("crawid.tmp", $crawler_id);
}
else
{
	$crawler_id = file_get_contents("crawid.tmp");
	$crawler->resume($crawler_id);
}
$crawler->go();
//$report = $crawler->getProcessReport();
/*
	$exclude = array(
	);
	foreach ($exclude as $ex) {
		$to_unset = array_search($ex, $urls);
		unset($urls[$to_unset]);
	}
*/
$urls = array();
if (file_exists('phpcrawl_tmp_' . $crawler_id . '/urlcache.db3')) {
$db = new SQLite3('phpcrawl_tmp_' . $crawler_id . '/urlcache.db3');
} else {
	die('File not found: ' . 'phpcrawl_tmp_' . $crawler_id . '/urlcache.db3');
}

$results = $db->query('SELECT url_rebuild FROM urls WHERE http_code=200');
while ($row = $results->fetchArray()) {
	$urls[] = $row['url_rebuild'];
}
$crawler->cleanup();

$mask = 'sitemap_*.xml';
array_map('unlink', glob($mask));

array_push($urls, 'http:// . ' . 'www.abamet.ru');
//$urls = range(1, 500000);

// $xml_files = array();
// $cur_xml_file = 0;
// $xml_files[] = array();
// for ($i=1; $i<count($urls); $i++) {
// 	array_push($xml_files[$cur_xml_file], str_replace('&', '&amp;', $urls[$i]));
// 	if (($i % 500) == 0) {
// 		$cur_xml_file++;
// 		$xml_files[] = array();
// 	}
// }
// foreach ($xml_files as $key => $xml_file) {
	
// }
$file = '<?xml version="1.0" encoding="UTF-8"?><urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">';
	foreach ($urls as $url) {
		$file .= '<url><loc>' . $url . '</loc></url>';
	}
	$file .= '</urlset>';
	file_put_contents('../sitemap.xml', $file);
/* $file = '<?xml version="1.0" encoding="UTF-8"?><sitemapindex xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">';*/
// for ($i=0; $i<=$cur_xml_file; $i++) {
// 	$file .= '<sitemap><loc>http://' . 'www.abamet.ru' . '/sitemap_' . ($i + 1) . '.xml</loc><lastmod>' . date(DATE_ATOM) . '</lastmod></sitemap>';
// }
// $file .= '</sitemapindex>';
file_put_contents('../sitemap.xml', $file);
unlink('crawid.tmp');
die('Sitemap.xml сгенерирован <a href="http://' . 'www.abamet.ru' . '">Перейти на сайт</a>');
?>